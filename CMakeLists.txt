cmake_minimum_required(VERSION 3.20)

project(geodude)

enable_testing()

find_package(GTest)
find_package(Doxygen)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

set(CMAKE_CXX_CLANG_TIDY "clang-tidy")

add_subdirectory(src)
add_subdirectory(app)
add_subdirectory(test)

set(CMAKE_EXPORT_COMPILE_COMMANDS true)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})
