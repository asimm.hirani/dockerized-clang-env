#include "geodude/geodude.hpp"

unsigned int GeodudeBasic::numInstances;

void GeodudeBasic::printHello() const {
    std::cout << "Hello from instance " << instance << std::endl;
}