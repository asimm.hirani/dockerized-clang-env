Geodude

This is not geodude, this is just geodude's build ecosystem.

Currently includes the following:
- Microsoft DevContainer
- .vscode c/cpp properties setup
- Gitlab CI pipelines
- Google Test/CTest integration and examples
- /build/bin/ binary directory
- /build/lib/ archive/library directory
- /include/ Include directory
- Clang-tidy setup
- 