#pragma once

#include <iostream>

class GeodudeBasic {
    static unsigned int numInstances;
    unsigned int instance = 0;
public:
    GeodudeBasic() {
        numInstances++;
        instance = numInstances;
    }
    ~GeodudeBasic() {
        // Don't do anything
    }

    void printHello() const;
};